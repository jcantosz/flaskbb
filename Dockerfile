FROM python
COPY . .
RUN make devconfig \
 && make install

EXPOSE 5000
CMD make run
